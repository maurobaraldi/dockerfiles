### Building image

`docker build -t simple-http:latest .`

### Running container

`docker run -d -v /local/path/:/opt/app -p <local_port>:8000 simplehttp

### Tests

curl http://0.0.0.0:<local_port>
