### Building image

`docker-compose build`

### Running container

`docker-compoe up -d --force-recreate`

### Tests

To see simple HTTP server test.

`curl http://0.0.0.0`

To see Kibana interface

http://localhost:5601
