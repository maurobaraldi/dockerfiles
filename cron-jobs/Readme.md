### Building image

`docker build -t cron-job:latest .`

### Running container

Adding `--log-driver` option to able output to container log.

`docker run -d --log-driver local cron-job:latest`

### Tests

Be patient, it may delay until 2 minutes before start to log.

`dokcer logs -f <container_id>`
